package queries;

public class CarsQuery {
    public static final String ADD_CARS = "INSERT INTO cars (brand, model, engine, production, body_type, seats, doors, speed, rent_amount) VALUES(? , ? , ? , ? , ? , ? , ? , ? , ?)";
    public static final String UPDATE_CARS_BY_ID = "UPDATE cars set brand = ?, model = ?, engine = ?,production = ?,body_type = ?, seats = ? , doors = ?, speed = ?, rent_amount = ?, status = ? WHERE id = ? and status =1";
    public static final String DELETE_CARS_BY_ID = "UPDATE cars SET status = 0 WHERE id = ? and status = 1";
    public static final String SHOW_CARS = "SELECT * FROM cars WHERE status = 1";
}
