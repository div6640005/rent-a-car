package repository;

import model.Cars;

public interface CustomerRepository {
    boolean addCustomer(Cars cars);
    void showCustomer();
    boolean deleteCustomer(int id);
    boolean updateCustomer(int id);
}
