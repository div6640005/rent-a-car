package repository;

import model.Cars;

public interface CarsRepository {
    boolean addCar(Cars cars);
    void showCars();
    boolean deleteCars(int id);
    boolean updateCars(int id, Cars cars);
}
