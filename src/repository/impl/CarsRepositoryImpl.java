package repository.impl;

import model.Cars;
import queries.CarsQuery;
import repository.CarsRepository;
import repository.DbConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class CarsRepositoryImpl extends DbConnection implements CarsRepository {
    @Override
    public boolean addCar(Cars cars) {
        try (Connection connection = connectionDb()) {
            PreparedStatement statement = connection.prepareStatement(CarsQuery.ADD_CARS);
            statement.setString(1, cars.getBrand());
            statement.setString(2, cars.getModel());
            statement.setString(3, cars.getEngine());
            statement.setInt(4, cars.getProduction());
            statement.setString(5, cars.getBody_type());
            statement.setInt(6, cars.getSeats());
            statement.setInt(7, cars.getDoors());
            statement.setInt(8, cars.getSpeed());
            statement.setInt(9, cars.getRent_amount());
            int count = statement.executeUpdate();
            return count > 0;
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public void showCars() {
        try (Connection connection = connectionDb()) {
            PreparedStatement statement = connection.prepareStatement(CarsQuery.SHOW_CARS);
            ResultSet resultSet = statement.executeQuery();
            List<Cars> cars = new LinkedList<>();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String brand = resultSet.getString("brand");
                String model = resultSet.getString("model");
                String engine = resultSet.getString("engine");
                int production = resultSet.getInt("production");
                String body_type = resultSet.getString("body_type");
                int seats = resultSet.getInt("seats");
                int doors = resultSet.getInt("doors");
                int speed = resultSet.getInt("speed");
                int rent_amount = resultSet.getInt("rent_amount");
                cars.add(new Cars(id, brand, model, engine, production, body_type, seats, doors, speed, rent_amount));
            }
            for (int i = 0; i < cars.size(); i++) {
                System.out.println(cars.get(i));
            }
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean deleteCars(int id) {
        try (Connection connection = connectionDb()) {
            PreparedStatement statement = connection.prepareStatement(CarsQuery.DELETE_CARS_BY_ID);
            statement.setInt(1, id);
            int value = statement.executeUpdate();
            return value > 0;
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean updateCars(int id, Cars Cars) {
        try (Connection connection = connectionDb()) {
            PreparedStatement statement = connection.prepareStatement(CarsQuery.UPDATE_CARS_BY_ID);
            statement.setString(1,Cars.getBrand());
            statement.setString(2,Cars.getModel());
            statement.setString(3,Cars.getEngine());
            statement.setInt(4,Cars.getProduction());
            statement.setString(5,Cars.getBody_type());
            statement.setInt(6,Cars.getSeats());
            statement.setInt(7,Cars.getDoors());
            statement.setInt(8,Cars.getSpeed());
            statement.setInt(9,Cars.getRent_amount());
            statement.setInt(10,Cars.getStatus());
            statement.setInt(11,Cars.getId());
            int value = statement.executeUpdate();
            return value>0;
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
