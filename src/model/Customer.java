package model;

public class Customer {
    private int id;
    private String full_name;
    private String birthdate;
    private String fin;
    private String serial_number;
    private int car_id;

    public Customer(int id, String full_name, String birthdate, String fin, String serial_number, int car_id) {
        this.id = id;
        this.full_name = full_name;
        this.birthdate = birthdate;
        this.fin = fin;
        this.serial_number = serial_number;
        this.car_id = car_id;
    }

    public Customer(String full_name, String birthdate, String fin, String serial_number, int car_id) {
        this.full_name = full_name;
        this.birthdate = birthdate;
        this.fin = fin;
        this.serial_number = serial_number;
        this.car_id = car_id;
    }

    public Customer() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getFin() {
        return fin;
    }

    public void setFin(String fin) {
        this.fin = fin;
    }

    public String getSerial_number() {
        return serial_number;
    }

    public void setSerial_number(String serial_number) {
        this.serial_number = serial_number;
    }

    public int getCar_id() {
        return car_id;
    }

    public void setCar_id(int car_id) {
        this.car_id = car_id;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", full_name='" + full_name + '\'' +
                ", birthdate='" + birthdate + '\'' +
                ", fin='" + fin + '\'' +
                ", serial_number='" + serial_number + '\'' +
                ", car_id=" + car_id +
                '}';
    }
}
