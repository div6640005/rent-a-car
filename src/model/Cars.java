package model;

public class Cars {
    private int id;
    private String brand;
    private String model;
    private String engine;
    private int production;
    private String body_type;
    private int seats;
    private int doors;
    private int speed;
    private int rent_amount;
    private int status;

    public Cars(int id, String brand, String model, String engine, int production, String body_type, int seats, int doors, int speed, int rent_amount) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.engine = engine;
        this.production = production;
        this.body_type = body_type;
        this.seats = seats;
        this.doors = doors;
        this.speed = speed;
        this.rent_amount = rent_amount;
    }

    public int getStatus() {
        return status;
    }

    public Cars(String brand, String model, String engine, int production, String body_type, int seats, int doors, int speed, int rent_amount) {
        this.brand = brand;
        this.model = model;
        this.engine = engine;
        this.production = production;
        this.body_type = body_type;
        this.seats = seats;
        this.doors = doors;
        this.speed = speed;
        this.rent_amount = rent_amount;
    }

    public Cars() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public int getProduction() {
        return production;
    }

    public void setProduction(int production) {
        this.production = production;
    }

    public String getBody_type() {
        return body_type;
    }

    public void setBody_type(String body_type) {
        this.body_type = body_type;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public int getDoors() {
        return doors;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getRent_amount() {
        return rent_amount;
    }

    public void setRent_amount(int rent_amount) {
        this.rent_amount = rent_amount;
    }

    @Override
    public String toString() {
        return "Cars{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", engine='" + engine + '\'' +
                ", production=" + production +
                ", body_type='" + body_type + '\'' +
                ", seats=" + seats +
                ", doors=" + doors +
                ", speed=" + speed +
                ", rent_amount=" + rent_amount +
                '}';
    }
}
